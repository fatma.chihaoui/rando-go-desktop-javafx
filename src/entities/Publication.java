/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author user
 */
public class Publication {
    
    private int id;
    private String path;
    private int id_rando;
    private String type_pub;
    private String login_user;

    public Publication(int id, String path, int id_rando, String type_pub, String login_user) {
        this.id = id;
        this.path = path;
        this.id_rando = id_rando;
        this.type_pub = type_pub;
        this.login_user = login_user;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return the id_rando
     */
    public int getId_rando() {
        return id_rando;
    }

    /**
     * @param id_rando the id_rando to set
     */
    public void setId_rando(int id_rando) {
        this.id_rando = id_rando;
    }

    /**
     * @return the type_pub
     */
    public String getType_pub() {
        return type_pub;
    }

    /**
     * @param type_pub the type_pub to set
     */
    public void setType_pub(String type_pub) {
        this.type_pub = type_pub;
    }

    /**
     * @return the login_user
     */
    public String getLogin_user() {
        return login_user;
    }

    /**
     * @param login_user the login_user to set
     */
    public void setLogin_user(String login_user) {
        this.login_user = login_user;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Publication other = (Publication) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
    
    
    
    
    
    
    
    
}
