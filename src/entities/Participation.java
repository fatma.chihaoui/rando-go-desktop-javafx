package entities;

import java.sql.Date;
import java.util.Objects;

/**
 *
 * @author aymoun
 */
public class Participation {
    
    public int id;
    public Date date_part;
    public int id_rando;
    public String login_user;

    public Participation() {
    }

    public Participation( int id, Date date_part, int id_rando, String login_user) {
        this.id = id;
        this.date_part = date_part;
        this.id_rando = id_rando;
        this.login_user = login_user;
    }

    public Date getDate_part() {
        return date_part;
    }

    public int getId() {
        return id;
    }

    public int getId_rando() {
        return id_rando;
    }

    public String getLogin_user() {
        return login_user;
    }

    public void setDate_part(Date date_part) {
        this.date_part = date_part;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setId_rando(int id_rando) {
        this.id_rando = id_rando;
    }

    public void setLogin_user(String login_user) {
        this.login_user = login_user;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + this.id;
        hash = 73 * hash + Objects.hashCode(this.date_part);
        hash = 73 * hash + this.id_rando;
        hash = 73 * hash + Objects.hashCode(this.login_user);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Participation other = (Participation) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.id_rando != other.id_rando) {
            return false;
        }
        if (!Objects.equals(this.login_user, other.login_user)) {
            return false;
        }
        if (!Objects.equals(this.date_part, other.date_part)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Participation{" + "id=" + id + ", date_part=" + date_part + ", id_rando=" + id_rando + 
                ", login_user=" + login_user + '}';
    }

   
    
}