/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Objects;

/**
 *
 * @author rihab_pc
 */
public class Guide {
    public String login;
    public int id_dest;
    public String nom;
    public String prenom;
    public String numtel;
    public String mail;
    public String genre;
    public String etat_profil;
    public int disponibilite;
    
    public Guide(){}

    public Guide(String login, int id_dest, String nom, String prenom, String numtel, String mail, String genre, String etat_profil, int disponibilite) {
        this.login = login;
        this.id_dest = id_dest;
        this.nom = nom;
        this.prenom = prenom;
        this.numtel = numtel;
        this.mail = mail;
        this.genre = genre;
        this.etat_profil = etat_profil;
        this.disponibilite = disponibilite;
    }

    public Guide(String login, int id_dest, String nom, String prenom, String numtel, String mail, String genre, String etat_profil) {
        this.login = login;
        this.id_dest = id_dest;
        this.nom = nom;
        this.prenom = prenom;
        this.numtel = numtel;
        this.mail = mail;
        this.genre = genre;
        this.etat_profil = etat_profil;
    }
    
    

    public String getLogin() {
        return login;
    }

    public int getId_dest() {
        return id_dest;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getNumtel() {
        return numtel;
    }

    public String getMail() {
        return mail;
    }

    public String getGenre() {
        return genre;
    }

    public String getEtat_profil() {
        return etat_profil;
    }

    public int getDisponibilite() {
        return disponibilite;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setId_dest(int id_dest) {
        this.id_dest = id_dest;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setNumtel(String numtel) {
        this.numtel = numtel;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setEtat_profil(String etat_profil) {
        this.etat_profil = etat_profil;
    }

    public void setDisponibilite(int disponibilite) {
        this.disponibilite = disponibilite;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Guide other = (Guide) obj;
        if (!Objects.equals(this.login, other.login)) {
            return false;
        }
        if (!Objects.equals(this.nom, other.nom)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Guide{" + "login=" + login + ", id_dest=" + id_dest + ", nom=" + nom + ", prenom=" + prenom + ", numtel=" + numtel + ", mail=" + mail + ", genre=" + genre + ", etat_profil=" + etat_profil + ", disponibilite=" + disponibilite + '}';
    }
    
    
    
    
    
}
