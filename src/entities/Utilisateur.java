/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Date;
import java.util.Objects;

/**
 *
 * @author rihab_pc
 */
public class Utilisateur {
    public int id;
 public String username;
 public String nom;
 public Date date_naissance;
 public String email;
 public String password;
 public String etat;
 private String role;
 
 public Utilisateur(){}

    public Utilisateur(String username, String nom, Date date_naissance, String email, String password, String etat, String role) {
        this.username = username;
        this.nom = nom;
        this.date_naissance = date_naissance;
        this.email = email;
        this.password = password;
        this.etat = etat;
        this.role = role;
    }

    @Override
    public String toString() {
        return "Utilisateur{" + "id=" + id + ", username=" + username + ", nom=" + nom + ", date_naissance=" + date_naissance + ", email=" + email + ", password=" + password + ", etat=" + etat + ", role=" + role + '}';
    }

 
 
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDate_naissance() {
        return date_naissance;
    }

    public void setDate_naissance(Date date_naissance) {
        this.date_naissance = date_naissance;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

 
 
 
}
