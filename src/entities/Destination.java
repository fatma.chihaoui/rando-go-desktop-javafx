/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author yes-man
 */
public class Destination {
    
    private int id;
    private String nom;
    private Localisation pointDeLocalisation;
    
    public String toString(){
        return "Destination{"
                + "id:"+id
                + ",nom:"+nom
                + "localisation:"+pointDeLocalisation
                + "}";
    }

    public Destination(int id, String nom, Localisation pointDeLocalisation) {
        this.id = id;
        this.nom = nom;
        this.pointDeLocalisation = pointDeLocalisation;
    }
    
    

    public int getId() {
        return id;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Localisation getPointDeLocalisation() {
        return pointDeLocalisation;
    }

    public void setPointDeLocalisation(Localisation pointDeLocalisation) {
        this.pointDeLocalisation = pointDeLocalisation;
    }
    
        
}
