/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Date;
import java.util.Objects;

/**
 *
 * @author rihab_pc
 */
public class  ConnectedUser {
    public static int id;
 public static String username;
 public static String nom;
 public static Date date_naissance;
 public static String email;
 public static String etat;
 private static String role;
 



    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        ConnectedUser.id = id;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        ConnectedUser.username = username;
    }

    public static String getNom() {
        return nom;
    }

    public static void setNom(String nom) {
        ConnectedUser.nom = nom;
    }

    public static Date getDate_naissance() {
        return date_naissance;
    }

    public static void setDate_naissance(Date date_naissance) {
        ConnectedUser.date_naissance = date_naissance;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        ConnectedUser.email = email;
    }

    public static String getEtat() {
        return etat;
    }

    public static void setEtat(String etat) {
        ConnectedUser.etat = etat;
    }

    public static String getRole() {
        return role;
    }

    public static void setRole(String role) {
        ConnectedUser.role = role;
    }

 
 
}
