/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author yes-man
 */
public class TypeRandonnee {
    
    private int id;
    private String type;

    public TypeRandonnee(int id, String type) {
        this.id = id;
        this.type = type;
    }
    

    public int getId() {
        return id;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
    
    
    
}
