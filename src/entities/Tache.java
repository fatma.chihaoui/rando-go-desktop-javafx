/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Date;
import java.util.Calendar;

/**
 *
 * @author yes-man
 */
public class Tache {
    
    private int id;
    private String Description;
    private Calendar heureDebut;
    private Calendar heureFin;
    private Localisation pointDeLocalisation;
    
    public Tache(){
    }

    public Tache(int aInt, String string, Calendar instance, Calendar instance0, Localisation insertedLocalisation) {
        this.id=aInt;
        this.Description=string;
        this.heureDebut=instance;
        this.heureFin=instance0;
        this.pointDeLocalisation=insertedLocalisation;
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public Calendar getHeureDebut() {
        return heureDebut;
    }

    public void setHeureDebut(Calendar heureDebut) {
        this.heureDebut = heureDebut;
    }

    public Calendar getHeureFin() {
        return heureFin;
    }

    public void setHeureFin(Calendar heureFin) {
        this.heureFin = heureFin;
    }

    public Localisation getPointDeLocalisation() {
        return pointDeLocalisation;
    }

    public void setPointDeLocalisation(Localisation pointDeLocalisation) {
        this.pointDeLocalisation = pointDeLocalisation;
    }

    @Override
    public String toString() {
        return "Tache{" + "id=" + id + ", Description=" + Description + ", heureDebut=" + heureDebut + ", heureFin=" + heureFin + ", pointDeLocalisation=" + pointDeLocalisation + '}';
    }
    
}
