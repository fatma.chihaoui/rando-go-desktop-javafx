/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author user
 */
public class Evaluation {
    
    private int id ;
    private int id_rando;
    private int note_orga;
    private int note_rando;
    private String login_user;

    public Evaluation(int id, int id_rando, int note_orga, int note_rando, String login_user) {
        this.id = id;
        this.id_rando = id_rando;
        this.note_orga = note_orga;
        this.note_rando = note_rando;
        this.login_user = login_user;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + this.getId();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Evaluation other = (Evaluation) obj;
        if (this.getId() != other.getId()) {
            return false;
        }
        return true;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the id_rando
     */
    public int getId_rando() {
        return id_rando;
    }

    /**
     * @param id_rando the id_rando to set
     */
    public void setId_rando(int id_rando) {
        this.id_rando = id_rando;
    }

    /**
     * @return the note_orga
     */
    public int getNote_orga() {
        return note_orga;
    }

    /**
     * @param note_orga the note_orga to set
     */
    public void setNote_orga(int note_orga) {
        this.note_orga = note_orga;
    }

    /**
     * @return the note_rando
     */
    public int getNote_rando() {
        return note_rando;
    }

    /**
     * @param note_rando the note_rando to set
     */
    public void setNote_rando(int note_rando) {
        this.note_rando = note_rando;
    }

    /**
     * @return the login_user
     */
    public String getLogin_user() {
        return login_user;
    }

    /**
     * @param login_user the login_user to set
     */
    public void setLogin_user(String login_user) {
        this.login_user = login_user;
    }

    @Override
    public String toString() {
        return "Evaluation{" + "id=" + id + ", id_rando=" + id_rando + ", note_orga=" + note_orga + ", note_rando=" + note_rando + ", login_user=" + login_user + '}';
    }
    
    
    
    
    
    
}
