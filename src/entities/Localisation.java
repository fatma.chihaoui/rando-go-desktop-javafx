/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author yes-man
 */
public class Localisation {
   
    private int id;
    private double lattitude;
    private double longitude;
    @Override
    public String toString() {
        return "Localisation{" + "id=" + id + ", lattitude=" + lattitude + ", longitude=" + longitude + '}';
    }
   

    public Localisation(int id, double lattitude, double longitude) {
        this.id = id;
        this.lattitude = lattitude;
        this.longitude = longitude;
    }
    
    

    public int getId() {
        return id;
    }

    public double getLattitude() {
        return lattitude;
    }

    public void setLattitude(double lattitude) {
        this.lattitude = lattitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
   
}
