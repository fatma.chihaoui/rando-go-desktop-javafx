/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Objects;
import java.sql.Date;
/**
 *
 * @author rihab_pc
 */
public class Administrateur {
    public String Login;
    public String password;
    public String nom;
    public String prenom;
    public Date date_naissance;
    public String mail;
    public String numtel;
    public String etat;
    
    public Administrateur(){}

    public Administrateur(String Login, String password, String nom, String prenom, Date date_naissance, String mail, String numtel, String etat) {
        this.Login = Login;
        this.password = password;
        this.nom = nom;
        this.prenom = prenom;
        this.date_naissance = date_naissance;
        this.mail = mail;
        this.numtel = numtel;
        this.etat = etat;
    }

    public String getLogin() {
        return Login;
    }

    public String getPassword() {
        return password;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Date getDate_naissance() {
        return date_naissance;
    }

    public String getMail() {
        return mail;
    }

    public String getNumtel() {
        return numtel;
    }

    public String getEtat() {
        return etat;
    }

    public void setLogin(String Login) {
        this.Login = Login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setDate_naissance(Date date_naissance) {
        this.date_naissance = date_naissance;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setNumtel(String numtel) {
        this.numtel = numtel;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.Login);
        hash = 41 * hash + Objects.hashCode(this.password);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Administrateur other = (Administrateur) obj;
        if (!Objects.equals(this.Login, other.Login)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "administrateur{" + "Login=" + Login + ", password=" + password + ", nom=" + nom + ", prenom=" + prenom + ", date_naissance=" + date_naissance + ", mail=" + mail + ", numtel=" + numtel + ", etat=" + etat + '}';
    }
    
    
    
    
    
    
}
