/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author yes-man
 */
public class RandonneeImage {
    
    private int id;
    private String path;

    public RandonneeImage(int id, String path) {
        this.id = id;
        this.path = path;
    }

    public RandonneeImage(String absolutePath) {
        this.path=absolutePath;
    }

    @Override
    public String toString() {
        return "RandonneeImage{" + "id=" + id + ", path=" + path + '}';
    }

    
    
    public int getId() {
        return id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    
    
}
