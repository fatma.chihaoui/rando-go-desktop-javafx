/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Date;

/**
 *
 * @author user
 */
public class Commentaire {
    
    
    private int id ;
    private int login_user;
    private String username;
    private Date date_creation;
    private String contenu;
    private int id_rando;

    public Commentaire(int id, int login_user, Date date_creation, String contenu, int id_rando,String username) {
        this.id = id;
        this.login_user = login_user;
        this.date_creation = date_creation;
        this.contenu = contenu;
        this.id_rando = id_rando;
        this.username=username;
    }

    public Commentaire(){    }

  

  

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.getId();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Commentaire other = (Commentaire) obj;
        if (this.getId() != other.getId()) {
            return false;
        }
        return true;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the login_user
     */
    public int getLogin_user() {
        return login_user;
    }

    /**
     * @param login_user the login_user to set
     */
    public void setLogin_user(int login_user) {
        this.login_user = login_user;
    }

    /**
     * @return the date_creation
     */
    public Date getDate_creation() {
        return date_creation;
    }

    /**
     * @param date_creation the date_creation to set
     */
    public void setDate_creation(Date date_creation) {
        this.date_creation = date_creation;
    }

    /**
     * @return the contenu
     */
    public String getContenu() {
        return contenu;
    }

    /**
     * @param contenu the contenu to set
     */
    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    /**
     * @return the id_rando
     */
    public int getId_rando() {
        return id_rando;
    }

    /**
     * @param id_rando the id_rando to set
     */
    public void setId_rando(int id_rando) {
        this.id_rando = id_rando;
    }

    @Override
    public String toString() {
        return "Commentaire{" + "id=" + id + ", login_user=" + login_user + ", username=" + username + ", date_creation=" + date_creation + ", contenu=" + contenu + ", id_rando=" + id_rando + '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

   
            


            
            
            
    
}
