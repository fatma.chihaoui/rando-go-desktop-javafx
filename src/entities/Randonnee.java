/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Date;
import java.util.List;

/**
 *
 * @author yes-man
 */
public class Randonnee {
    
    private int id;
    private String Titre;
    private String description;
    private int nbPlacesMin;
    private int nbPlacesMax;
    private Date date_rando;
    private float nbKilometres;
    private int etat;
    public int prix;
    private List<RandonneeImage> listImages;
    private List<Tache> listTaches;
    private Destination destination;
    private TypeRandonnee typeRandonnee;
    private Utilisateur loginOrganisateur;

    public Randonnee(int id, String Titre, String description, int nbPlacesMin, int nbPlacesMax, Date date_rando, float nbKilometres,int etat, List<RandonneeImage> listImages, List<Tache> listTaches, Destination destination, TypeRandonnee typeRandonnee, Utilisateur loginOrganisateur,int prix) {
        this.id = id;
        this.Titre = Titre;
        this.description = description;
        this.nbPlacesMin = nbPlacesMin;
        this.nbPlacesMax = nbPlacesMax;
        this.date_rando = date_rando;
        this.nbKilometres = nbKilometres;
        this.etat = etat;
        this.listImages = listImages;
        this.listTaches = listTaches;
        this.destination = destination;
        this.typeRandonnee = typeRandonnee;
        this.loginOrganisateur = loginOrganisateur;
        this.prix=prix;
    }

    public Randonnee() {
        
    }

    public Utilisateur getLoginOrganisateur() {
        return loginOrganisateur;
    }

    public void setLoginOrganisateur(Utilisateur loginOrganisateur) {
        this.loginOrganisateur = loginOrganisateur;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return Titre;
    }

    public void setTitre(String Titre) {
        this.Titre = Titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNbPlacesMin() {
        return nbPlacesMin;
    }

    public void setNbPlacesMin(int nbPlacesMin) {
        this.nbPlacesMin = nbPlacesMin;
    }

    public int getNbPlacesMax() {
        return nbPlacesMax;
    }

    public void setNbPlacesMax(int nbPlacesMax) {
        this.nbPlacesMax = nbPlacesMax;
    }

    public Date getDate_rando() {
        return date_rando;
    }

    public void setDate_rando(Date date_rando) {
        this.date_rando = date_rando;
    }

    public float getNbKilometres() {
        return nbKilometres;
    }

    public void setNbKilometres(float nbKilometres) {
        this.nbKilometres = nbKilometres;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public List<RandonneeImage> getListImages() {
        return listImages;
    }

    public void setListImages(List<RandonneeImage> listImages) {
        this.listImages = listImages;
    }

    public List<Tache> getListTaches() {
        return listTaches;
    }

    public void setListTaches(List<Tache> listTaches) {
        this.listTaches = listTaches;
    }

    public TypeRandonnee getTypeRandonnee() {
        return typeRandonnee;
    }

    public void setTypeRandonnee(TypeRandonnee typeRandonnee) {
        this.typeRandonnee = typeRandonnee;
    }

    @Override
    public String toString() {
        return "Randonnee{" + "id=" + id + ", Titre=" + Titre + ", description=" + description + ", nbPlacesMin=" + nbPlacesMin + ", nbPlacesMax=" + nbPlacesMax + ", date_rando=" + date_rando + ", nbKilometres=" + nbKilometres + ", etat=" + etat + ", listImages=" + listImages + ", listTaches=" + listTaches + ", destination=" + destination + ", typeRandonnee=" + typeRandonnee + ", loginOrganisateur=" + loginOrganisateur + '}';
    }
    
    
    
    
    
    
    
    
    
}
