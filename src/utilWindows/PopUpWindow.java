/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilWindows;

import entities.Randonnee;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author yes-man
 */
public class PopUpWindow 
{
    private Stage window;
        public PopUpWindow (String titre,String msg){


                window =new Stage();
                window.initModality(Modality.APPLICATION_MODAL);
                
                    
                window.setTitle(titre);
                window.setMaxHeight(250);
                window.setMaxWidth(350);
                window.setMinHeight(250);
                window.setMinWidth(350);
                
                Label lab=new Label(msg);
                Button closebtn=new Button("ok");
                closebtn.setOnAction(a->window.close());
                
                VBox vboxlayout=new VBox(20,lab,closebtn);
                vboxlayout.setAlignment(Pos.CENTER);
                window.setScene(new Scene(vboxlayout));
                window.showAndWait();
            }
    
}
