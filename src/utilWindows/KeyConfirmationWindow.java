/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilWindows;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import entities.Randonnee;
import entities.RandonneeImage;
import java.util.ArrayList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import services.RandonneeDAO;
import utils.MailService;
import utils.SecuretyNumberGenerator;
import utils.UploadClass;

/**
 *
 * @author yes-man
 */
public class KeyConfirmationWindow {
    
    Stage stage1;
    String confirmationCode;
    Randonnee rando;
    
    public KeyConfirmationWindow(Randonnee randonnee){
        
        
        rando=randonnee;
        
       stage1=new Stage();
       stage1.initModality(Modality.APPLICATION_MODAL);
Label label1=new Label("un code de validation a ete envoyer a votre email");
Label label2=new Label("copier le et coller ici");
Label msg=new Label();
JFXTextField textfield=new JFXTextField();
JFXButton button=new JFXButton("confirmer");
textfield.setAlignment(Pos.CENTER);

VBox vboxlayout=new VBox(20,label1,label2,textfield,button,msg);
                vboxlayout.setAlignment(Pos.CENTER);
                vboxlayout.setMinSize(600,400);
                vboxlayout.setStyle("-fx-background-color:#ffffff");
Scene scene=new Scene(vboxlayout);

stage1.setScene(scene);


button.setOnAction(eh->{
if(textfield.getText().equals(confirmationCode)){
    System.out.println("confirmation code is correct");
     int compteur=0;
        for(RandonneeImage i : randonnee.getListImages()){
            System.out.println("uploading "+i.getPath());
            UploadClass.upload(i.getPath(),"image_"+randonnee.getTitre().replaceAll(" ","_")+"_"+compteur+".jpg");
            ((ArrayList<RandonneeImage>)randonnee.getListImages()).get(randonnee.getListImages().indexOf(i)).setPath("image_"+randonnee.getTitre().replaceAll(" ","_")+"_"+compteur);
            compteur++;
        }
        
        new RandonneeDAO().ajouterRandonnee(randonnee);
        new PopUpWindow("randonnee ajouter","randonnee cree");
        stage1.close();
    
}else{
  msg.setText("code de validation incorrecte");
  
}   
});

    }
    
    public void show(){
        sendConfirmationCode(rando.getLoginOrganisateur().getEmail());
        stage1.showAndWait();
    }
    
    
    private void sendConfirmationCode(String email){
        (new Thread(()->{
        confirmationCode=SecuretyNumberGenerator.getInstance().getSecuretyNumber();
        (new MailService()).sendEMail(email,"confirmation Code",""
                + "copier ce code de securite et colle le dans l'application\n"
                +confirmationCode
                + "\n si vous n'avez pas ajouter une randonnee ignore ce mail");
        
        })).start();
        System.out.println("email end");
        
       
        
        
    }
    
    
}
