/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Utilisateur;
import javafx.collections.ObservableList;

/**
 *
 * @author rihab_pc
 */
public interface IUtilisateur {
    
  public void ajouterUtilisateur(Utilisateur util ); 
  public void DesactiverUtilisateur( String login);
  public void modifierUtilisateur(Utilisateur util );
  public Utilisateur findProduitByLogin(String login);
  public ObservableList<Utilisateur> listeUtilisateur();
  public Utilisateur findByLogin(String login);
  public boolean CheckLogin(String login, String password);
  public String FindIdByLogin(String login);
  public void ActivateCompte(Utilisateur util );
  public void UpdateImgpath(Utilisateur a);
  public void UpdateInfo(Utilisateur a);
  public void Updatepwd(String password, String login);
  public void UpdateEnable(String username, int x);
  
}
