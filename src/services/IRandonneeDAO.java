/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Randonnee;
import entities.RandonneeImage;
import java.util.List;

/**
 *
 * @author yes-man
 */
public interface IRandonneeDAO {
    
    public void ajouterRandonnee(Randonnee r);
    public void modifierRandonnee(Randonnee r);
    public List<Randonnee> affichertous(int etat);
    public void  modifierRandonneeImage(RandonneeImage l);
    public void  supprimerRandonneeImage(RandonneeImage l);
    public List<RandonneeImage> afficherTous_images(int id_randonnee);
}
