/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;



import entities.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.*;
import java.sql.Date;
import java.util.List;
/**
 *
 * @author user
 */
public class CommentaireDAO implements ICommentaireDAO {
    
     
     PreparedStatement st;
     
     
     public CommentaireDAO(){
     
          
         
     }
     
   
     @Override
     public Boolean AjouterCommentaire(Commentaire c)
    { 
        try {

            String req = "INSERT INTO commentaire (login_user,date,contenu,id_rando) VALUES(?,sysdate(),?,?)";
                 st = DbConnection.getInstance().getConnection().prepareStatement(req);                     
                 st.setInt(1, c.getLogin_user());
                 
                 st.setString(2, c.getContenu());
                 st.setInt(3, c.getId_rando());
                 st.executeUpdate();//insert update delete
        } catch (SQLException ex) {
            Logger.getLogger(CommentaireDAO.class.getName()).log(Level.SEVERE, null, ex);
            
            System.out.println("ma temchich ");
        }
        return true ;
    }
    
    
     @Override
   public void ModifierCommentaire (Commentaire c)
   {
       try {                
            
            
            String req = "UPDATE commentaire "
                    + "SET contenu=? where id=?";
                      
                st = DbConnection.getInstance().getConnection().prepareStatement(req);                     
                st.setString(1, c.getContenu());
                st.setInt(2, c.getId());
                st.executeUpdate();//insert update delete
                System.out.println("temchi");
        } catch (SQLException ex) {
            Logger.getLogger(CommentaireDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("ma temchich ");
        }
       
   
   }
   
     @Override
   public void SupprimerCommentaire(int id)
   {
   try {
            String req = "DELETE FROM commentaire WHERE id = ?";
            st = DbConnection.getInstance().getConnection().prepareStatement(req);
            st.setInt(1, id); 
            st.executeUpdate();
            System.out.println("tfasa5");
        } catch (SQLException ex) {
            Logger.getLogger(CommentaireDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("matfasa5ch");
        }
    }
   

     @Override
    public List<Commentaire> lister(int id_rando) {

        List<Commentaire> list = new ArrayList<>();
        ResultSet rs; // pour récupérer le résultat de select
        String req = "SELECT c.*,r.username FROM commentaire c join user r on c.login_user=r.id where id_rando="+id_rando;
        try {
            st = DbConnection.getInstance().getConnection().prepareStatement(req);
            rs = st.executeQuery(req);
            while (rs.next()) {
              Commentaire c = new Commentaire(rs.getInt("id"),rs.getInt("login_user"),
                                    rs.getDate("date"),rs.getString("contenu"),rs.getInt("id_rando"),rs.getString("username") );
                        
                       
                list.add(c);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CommentaireDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
/*
    @Override
    public List<Commentaire> ListerParIdRando(int id) {
       
        List<Commentaire> listeCommentaires = new ArrayList<>();
        Commentaire c=null;
         ResultSet rs; // pour récupérer le résultat de select
        String req = "SELECT * FROM commentaire WHERE id_rando =?";
        try {
            st = dataSource.getConnection().prepareStatement(req);
            st.setInt(1, id);
            rs = st.executeQuery(req);
            while (rs.next()) {
                c = new Commentaire();
                
                c.setId(rs.getInt("id"));
                c.setLogin_user(rs.getString("login_user"));
                c.setDate_creation(rs.getDate("date"));
                c.setContenu(rs.getString("contenu"));
              
                
                listeCommentaires.add(c);
                
                }             
                 return listeCommentaires;
                } catch (SQLException ex) {
            Logger.getLogger(PublicationDAO.class.getName()).log(Level.SEVERE, null, ex);
      
                }
        return null;
    
    }


*/

}  
             
                                                                             
          
             
        
   
   
   
   


       
   
    
  
    
