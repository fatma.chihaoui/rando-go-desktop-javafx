/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Localisation;

/**
 *
 * @author yes-man
 */
public interface ILocalisationDAO {
    public Localisation ajouter(Localisation l);
    public void  modifier(Localisation l);
    public void  supprimer(Localisation l);
    public Localisation  afficher(int id);
    
            
}
