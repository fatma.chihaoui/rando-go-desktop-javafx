/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.ConnectedUser;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import entities.Utilisateur;
import utils.DbConnection;

/**
 *
 * @author rihab_pc
 */
public class UtilisateurDAO {
 
    PreparedStatement ste;

    public UtilisateurDAO() {
      
    }  
    
  
    public boolean ajouterUtilisateur(Utilisateur util ) {
        try {
           
            String req = "INSERT INTO `randonnee`.`user` VALUES (NULL,?, ?, ?, ?, '1', NULL, '$2y$13$Fnqp3oh82k.fJata9XuRkeKnklr3ZEnTpzpdiadpVhjySGf6RciB6', NULL, NULL, NULL, 'a:0:{}', NULL, null, NULL, null, NULL,'avatar.png', 0, NULL, NULL, NULL)";
                    
            ste=DbConnection.getInstance().getConnection().prepareStatement(req);
              ste.setString(1,util.getUsername());
              ste.setString(2,util.getUsername());
              ste.setString(3,util.getEmail());
              ste.setString(4,util.getEmail());
              ste.executeUpdate();//insert update delete
              
              req="select * from user where username=?";
              ste=DbConnection.getInstance().getConnection().prepareStatement(req);
              ste.setString(1,util.getUsername());
              ResultSet rs=ste.executeQuery();
              rs.next();
              
              ConnectedUser.setId(rs.getInt("id"));
              ConnectedUser.setUsername(rs.getString("username"));
              ConnectedUser.setEmail(rs.getString("email"));
              return true;
        } catch (SQLException ex) {
              System.out.println("Erreur dans la requette Insert: ");
               ex.printStackTrace();
        }
        return false;
    }
      
    public void connectUser(String username,String pwd){
        
        
        try{
        String req="select * from user where username=? ";
        
        ste=DbConnection.getInstance().getConnection().prepareStatement(req);
              ste.setString(1,username);
              ResultSet rs=ste.executeQuery();
              rs.next();
              ConnectedUser.setId(rs.getInt("id"));
              ConnectedUser.setUsername(rs.getString("username"));
              ConnectedUser.setEmail(rs.getString("email"));
              
        }catch(Exception e){
            e.printStackTrace();
        }
        
    }
      
}
