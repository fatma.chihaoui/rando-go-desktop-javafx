/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Destination;
import entities.Localisation;
import entities.Randonnee;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DbConnection;

/**
 *
 * @author yes-man
 */
public class DestinationDAO implements IDestinationDAO{
    

    
    
    
    
    public Destination ajouter(Destination  d){
        
        try {
            Localisation insertedLocalisation=new LocalisationDAO().ajouter(d.getPointDeLocalisation());
            //insert new destination
            String req="insert into destination values(null,?,?)";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setString(1,d.getNom());
            pst.setInt(2,insertedLocalisation.getId());
            pst.executeUpdate();
            System.out.println("destination ajouter");
            //get the id of the new inserted destination
             req="select * from destination";
            pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            ResultSet rs=pst.executeQuery();
            rs.last();
            //return the new inserted destination with the id
            Destination des=new Destination(rs.getInt("id"),rs.getString("nom"),insertedLocalisation);
            
            return des;
            
        } catch (Exception ex) {
            Logger.getLogger(LocalisationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    return null;
    }
 
    public void  modifier(Destination l){
        try {
            String req="update destination set"
                    + " path=?"
                    + "where id=?";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setString(1,l.getNom());
            pst.setInt(2, l.getId());
            pst.executeUpdate();
            System.out.println("Destination modifier");
        } catch (Exception ex) {
            Logger.getLogger(LocalisationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
      
    public void  supprimer(Destination l){
        try {
            String req="delete from destination where id=?";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setInt(1, l.getId());
            pst.executeUpdate();
            new LocalisationDAO().supprimer(l.getPointDeLocalisation());
            System.out.println("destination supprimer");
        } catch (Exception ex) {
            Logger.getLogger(LocalisationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public Destination  afficher(int id){
        try {
            String req="select * from destination where id=?";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setInt(1,id);
            ResultSet rs=pst.executeQuery();
            rs.next();
            Destination d=new Destination(rs.getInt("id"), rs.getString("nom"),new LocalisationDAO().afficher(rs.getInt("id_localisation")));

            return d;
        } catch (Exception ex) {
            Logger.getLogger(LocalisationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
        public List<Destination>  affichertous(){
        try {
            List<Destination> list=new ArrayList<Destination>();
            String req="select * from destination ";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            ResultSet rs=pst.executeQuery();
            LocalisationDAO localisationService=new LocalisationDAO();
            while(rs.next()){
                list.add(new Destination(rs.getInt("id"), rs.getString("nom"),localisationService.afficher(rs.getInt("id_localisation"))));
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(LocalisationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
