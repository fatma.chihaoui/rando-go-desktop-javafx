/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.TypeRandonnee;
import java.util.List;

/**
 *
 * @author yes-man
 */
public interface ITypeRandonneeDAO {
    
    public void  ajouter(TypeRandonnee  l);
    public void  modifier(TypeRandonnee l);
    public void  supprimer(TypeRandonnee l);
    public TypeRandonnee  afficher(int id);
    public List<TypeRandonnee>  affichertous();
    
}
