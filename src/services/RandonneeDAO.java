/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Destination;
import entities.Randonnee;
import entities.RandonneeImage;
import entities.Tache;
import entities.TypeRandonnee;
import entities.Utilisateur;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DbConnection;

/**
 *
 * @author yes-man
 */
public class RandonneeDAO implements IRandonneeDAO{
    
    public void ajouterRandonnee(Randonnee r){
        
        try {
            //this part is to add a new destination with a localisation
            Destination d=new DestinationDAO().ajouter(r.getDestination());

            //this part is to add a randonne
            String req="insert into randonnee values(null,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setString(1,r.getTitre());
            pst.setString(2,r.getDescription());
            pst.setInt(3,r.getNbPlacesMin());
            pst.setInt(4,r.getNbPlacesMax());
            pst.setDate(5,r.getDate_rando());
            pst.setFloat(6,r.getNbKilometres());
            pst.setInt(7,r.getEtat());
            pst.setInt(8,r.prix);
            pst.setInt(9,d.getId());
            pst.setInt(10,r.getTypeRandonnee().getId());
            pst.setInt(11,r.getLoginOrganisateur().id);

            pst.executeUpdate();
            
            //this part is to get the id of the randonnee
            req="select * from randonnee";
            pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            ResultSet rs=pst.executeQuery();
            rs.last();
            
            //this part is to add images from a randonnee
            for(RandonneeImage ri : r.getListImages()){
                req="insert into randonnee_images values (null,?,?)";
                pst=DbConnection.getInstance().getConnection().prepareStatement(req);
                pst.setString(1,ri.getPath());
                pst.setInt(2, rs.getInt("id"));
                pst.executeUpdate();
            }
            
            TacheDAO tacheservice=new TacheDAO();
            //this part is to add tasks for this randonnee
            for(Tache ta : r.getListTaches()){
                tacheservice.ajout(ta, rs.getInt("id"));
            }  
        } catch (SQLException ex) {
            Logger.getLogger(RandonneeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    public void modifierRandonnee(Randonnee r){
        try {
            String req="update randonnee set "
                    + "titre=?,"
                    + "description=?,"
                    + "nb_place_min=?,"
                    + "nb_place_max=?,"
                    + "date_rando=?,"
                    + "nb_kilometres=?,"
                    + "etat=?, "
                    +"prix=? "
                    + "where id=? ";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setString(1,r.getTitre());
            pst.setString(2,r.getDescription());
            pst.setInt(3,r.getNbPlacesMin());
            pst.setInt(4,r.getNbPlacesMax());
            pst.setDate(5,r.getDate_rando());
            pst.setFloat(6,r.getNbKilometres());
            pst.setInt(7,r.getEtat());
            pst.setInt(8, r.prix);
            pst.setInt(9, r.getId());
           
            pst.executeUpdate();
            TacheDAO tacheService=new TacheDAO();
            r.getListTaches().forEach((tache) -> {
                tacheService.modifier(tache);
            });
            
            
        } catch (SQLException ex) {
            Logger.getLogger(RandonneeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Randonnee> affichertous(int etat){
        
        try {
            List<Randonnee> listRandonnee=new ArrayList<>();
            String req="select * from randonnee where etat=?";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setInt(1, etat);
            ResultSet rs=pst.executeQuery();
            TypeRandonneeDAO typeRandoservice=new TypeRandonneeDAO();
            TacheDAO tacheService=new TacheDAO();
            while(rs.next()){
                Destination d=new DestinationDAO().afficher(rs.getInt("id_destination"));
                TypeRandonnee tr=typeRandoservice.afficher(rs.getInt("id_type_randonnee"));
                List<RandonneeImage> listImages=afficherTous_images(rs.getInt("id"));
                Utilisateur u=new Utilisateur();
                u.id=rs.getInt("login_organisateur");
                listRandonnee.add(
                new Randonnee(rs.getInt("id"),rs.getString("titre"),rs.getString("description"),rs.getInt("nb_place_min"),rs.getInt("nb_place_max"),rs.getDate("date_rando"),rs.getFloat("nb_kilometres"),rs.getInt("etat"),listImages,tacheService.afficherTousParIdRando(rs.getInt("id")), d, tr,u,rs.getInt("prix"))
                );
            }
            return listRandonnee;
        } catch (SQLException ex) {
            Logger.getLogger(RandonneeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    }
    
    public void supprimerRandonnee(Randonnee rando){
        try {
            String req="delete from randonnee where id=?";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setInt(1,rando.getId());
            pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RandonneeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }
    
    
    
    
    public void  modifierRandonneeImage(RandonneeImage l){
        try {
            String req="update randonnee_images set"
                    + " path=?"
                    + "where id=?";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setString(1,l.getPath());
            pst.setInt(2, l.getId());
            pst.setInt(2, l.getId());
            pst.executeUpdate();
            System.out.println("randonnee_images modifier");
        } catch (Exception ex) {
            Logger.getLogger(LocalisationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    public void  supprimerRandonneeImage(RandonneeImage l){
        try {
            String req="delete from randonnee_images where id=?";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setInt(1, l.getId());
            pst.executeUpdate();
            System.out.println("randonnee_images supprimer");
        } catch (Exception ex) {
            Logger.getLogger(LocalisationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<RandonneeImage> afficherTous_images(int id_randonnee){
        
        try {
            List<RandonneeImage> listImages=new ArrayList();
            String req="select * from randonnee_images where id_rando=? ";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setInt(1, id_randonnee);
            ResultSet rs=pst.executeQuery();
            while(rs.next()){
                listImages.add(new RandonneeImage(rs.getInt("id"),rs.getString("path")));
            }
            return listImages;
        } catch (SQLException ex) {
            Logger.getLogger(RandonneeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null; 
    }
    
}
