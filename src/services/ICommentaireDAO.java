/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Commentaire;
import entities.Randonnee;
import java.util.List;

/**
 *
 * @author user
 */
public interface ICommentaireDAO {
    
      public Boolean AjouterCommentaire(Commentaire c);
      public void ModifierCommentaire (Commentaire c);
      public void SupprimerCommentaire(int id);
      public List<Commentaire> lister(int r);
   //   public List<Commentaire> ListerParIdRando(int id_rando);
}
