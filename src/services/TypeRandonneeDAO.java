/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.TypeRandonnee;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DbConnection;

/**
 *
 * @author yes-man
 */
public class TypeRandonneeDAO implements ITypeRandonneeDAO{
    
    
    
    public void  ajouter(TypeRandonnee  l){
        try {
            String req="insert into type_randonnee values(null,?)";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setString(1,l.getType());
            pst.executeUpdate();
            System.out.println("type randonnee ajouter");
        } catch (Exception ex) {
            Logger.getLogger(LocalisationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void  modifier(TypeRandonnee l){
        try {
            String req="update type_randonnee set"
                    + " type=?"
                    + " where id=?";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setString(1,l.getType());
            pst.setInt(2, l.getId());
            pst.executeUpdate();
            System.out.println("type randonnee modifier");
        } catch (Exception ex) {
            Logger.getLogger(LocalisationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    public void  supprimer(TypeRandonnee l){
        try {
            String req="delete from type_randonnee where id=?";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setInt(1, l.getId());
            pst.executeUpdate();
            System.out.println("type randonnee supprimer");
        } catch (Exception ex) {
            Logger.getLogger(LocalisationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    public TypeRandonnee  afficher(int id){
        try {
            String req="select * from type_randonnee where id=?";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setInt(1,id);
            ResultSet rs=pst.executeQuery();
            rs.next();
            return new TypeRandonnee(rs.getInt("id"), rs.getString("type"));
            
        } catch (Exception ex) {
            Logger.getLogger(LocalisationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
        public List<TypeRandonnee>  affichertous(){
        try {
            List<TypeRandonnee> list=new ArrayList<TypeRandonnee>();
            String req="select * from type_randonnee ";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            ResultSet rs=pst.executeQuery();
            while(rs.next()){
                list.add(new TypeRandonnee(rs.getInt("id"), rs.getString("type")));
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(LocalisationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
  
    
}
