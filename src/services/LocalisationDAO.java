/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Localisation;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DbConnection;

/**
 *
 * @author yes-man
 */
public class LocalisationDAO implements ILocalisationDAO{
    
    
    public Localisation ajouter(Localisation l){
        try {
            String req="insert into localisation values(null,?,?)";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
           
            pst.setDouble(1,l.getLattitude());
            pst.setDouble(2, l.getLongitude());
            pst.executeUpdate();
            System.out.println("localisation ajouter");
            req="select * from localisation where "
                    + "lattitude=? "
                    + "and longitude=?";
            pst=DbConnection.getInstance().getConnection().prepareStatement(req);
           
            pst.setDouble(1,l.getLattitude());
            pst.setDouble(2, l.getLongitude());
            ResultSet rs=pst.executeQuery();
            rs.last();
            Localisation lo=new Localisation(rs.getInt("id"),rs.getDouble("lattitude"),rs.getDouble("longitude"));
            
            return lo;
            

            
        } catch (Exception ex) {
            Logger.getLogger(LocalisationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void  modifier(Localisation l){
        try {
            String req="update localisation set"
                    + "lattitude=?,"
                    + "longitude=? "
                    + "where id=?";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setDouble(1,l.getLattitude());
            pst.setDouble(2, l.getLongitude());
            pst.setInt(4, l.getId());
            pst.executeUpdate();
            System.out.println("localisation modifier");
        } catch (Exception ex) {
            Logger.getLogger(LocalisationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    public void  supprimer(Localisation l){
        try {
            String req="delete from localisation where id=?";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setInt(1, l.getId());
            pst.executeUpdate();
            System.out.println("localisation supprimer");
        } catch (Exception ex) {
            Logger.getLogger(LocalisationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Localisation  afficher(int id){
        try {
            String req="select * from localisation where id=?";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setInt(1,id);
            ResultSet rs=pst.executeQuery();
           
            if(rs.next()!=false){
                Localisation l=new Localisation(rs.getInt("id"),rs.getDouble("lattitude"),rs.getDouble("longitude"));
            return l;
            }
            return null;
            
        } catch (Exception ex) {
            Logger.getLogger(LocalisationDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
}
