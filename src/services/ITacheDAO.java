/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Tache;
import java.util.List;

/**
 *
 * @author yes-man
 */
public interface ITacheDAO {
    public Tache ajout(Tache tache,int id_randonnee);
    public List<Tache> afficherTousParIdRando(int idRando);
    public void modifier(Tache t);
    public void supprimer(Tache t);
    
    
}
