/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Localisation;
import entities.Tache;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DbConnection;

/**
 *
 * @author yes-man
 */
public class TacheDAO implements ITacheDAO{
    
    public Tache ajout(Tache tache,int id_randonnee){
        try {
            Localisation insertedLocalisation=null;
            String req;
            if(tache.getPointDeLocalisation()!=null){
                insertedLocalisation=new LocalisationDAO().ajouter(tache.getPointDeLocalisation());
                req="insert into taches values(null,?,?,?,?,?)";
            }else{
                req="insert into taches values(null,?,?,?,?,null)";
            }
            
            
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            
            pst.setTimestamp(1,new Timestamp(tache.getHeureDebut().getTimeInMillis()));
            pst.setTimestamp(2,new Timestamp(tache.getHeureFin().getTimeInMillis()));
            pst.setString(3, tache.getDescription());
            pst.setInt(4, id_randonnee);
            if(insertedLocalisation!=null){
                pst.setInt(5,insertedLocalisation.getId());
            }
            
            pst.executeUpdate();
            
            req="select * from taches";
            pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            ResultSet rs=pst.executeQuery();
            rs.last();
            Calendar cdebut=Calendar.getInstance();
               cdebut.setTimeInMillis(rs.getTimestamp("heure_debut").getTime());
               Calendar cfin=Calendar.getInstance();
               cfin.setTimeInMillis(rs.getTimestamp("heure_fin").getTime());
            Tache newTache=new Tache(rs.getInt("id"),rs.getString("description"),cdebut,cfin,insertedLocalisation);
            return newTache;
            
        } catch (Exception ex) {
            Logger.getLogger(TacheDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Tache> afficherTousParIdRando(int idRando){
        List<Tache> listTaches=new ArrayList();
        try {
            
            String req="select * from taches where id_rando="+idRando;
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            ResultSet rs=pst.executeQuery();
            while(rs.next()){
               Calendar cdebut=Calendar.getInstance();
               cdebut.setTimeInMillis(rs.getTimestamp("heure_debut").getTime());
               Calendar cfin=Calendar.getInstance();
               cfin.setTimeInMillis(rs.getTimestamp("heure_fin").getTime());
               listTaches.add(new Tache(rs.getInt("id"),rs.getString("description"),cdebut,cfin,new LocalisationDAO().afficher(rs.getInt("id_localisation"))));
            }
            return listTaches;
        } catch (Exception ex) {
            Logger.getLogger(TacheDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listTaches;
    }
    
    public void modifier(Tache t){
        try {
            String req="update taches set description=?"
                    + ",heure_debut=?"
                    + ",heure_fin=? "
                    + "where id=?";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setString(1,t.getDescription());
            pst.setInt(4, t.getId());
            pst.setTimestamp(2,new Timestamp(t.getHeureDebut().getTimeInMillis()));
            pst.setTimestamp(3,new Timestamp(t.getHeureFin().getTimeInMillis()));
            pst.executeUpdate();
            
            
        } catch (Exception ex) {
            Logger.getLogger(TacheDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
                
    }
    
    public void supprimer(Tache t){
        try {
            String req="delete from taches where id=?";
            PreparedStatement pst=DbConnection.getInstance().getConnection().prepareStatement(req);
            pst.setInt(1, t.getId());
            pst.executeUpdate();
            new LocalisationDAO().supprimer(t.getPointDeLocalisation());
        } catch (Exception ex) {
            Logger.getLogger(TacheDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
