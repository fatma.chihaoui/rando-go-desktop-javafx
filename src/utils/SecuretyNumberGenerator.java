/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 *
 * @author yes-man
 */
public class SecuretyNumberGenerator {
    
    private static SecuretyNumberGenerator instance;
    private static SecureRandom random;
    private SecuretyNumberGenerator(){
        random = new SecureRandom();
 //System.out.println();
    }
    
    public static SecuretyNumberGenerator getInstance(){
        if(instance==null){
            instance =new SecuretyNumberGenerator();
        }
        return instance;
    }
    
    public static String getSecuretyNumber(){
        return BigInteger.probablePrime(130, random).toString(32);
    }
    
    
}
