/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author yes-man
 */
public class UploadClass {
    
    public static void upload(String filePath,String upload_name){
        try {
            System.out.println("filepath "+filePath+" upload_name "+upload_name);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)new URL("http://localhost/pidev/upload.php?filename="+upload_name).openConnection();
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setRequestMethod("POST");
            OutputStream os = httpUrlConnection.getOutputStream();
            Thread.sleep(1000);
            BufferedInputStream fis = new BufferedInputStream(new FileInputStream(filePath));
            
            int ii=1;
            while(ii!=-1) {
                ii=fis.read();
                os.write(ii);
            }
            
            os.close();
           
//servers response
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            httpUrlConnection.getInputStream()));
            
            /*
            String s = null;
            while ((s = in.readLine()) != null) { 
            }
            in.close();
            */

            fis.close();
        } catch (Exception ex) {
            Logger.getLogger(UploadClass.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
}
