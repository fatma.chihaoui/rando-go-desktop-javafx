/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author yes-man
 */
public class DbConnection {
    
    private static String url="jdbc:mysql://localhost:3306/randonnee";
    private static String login="root";
    private static String pwd="";
    
    private static Connection connection;
    private static DbConnection instance;
    
    private DbConnection(){
        try{
        this.connection= DriverManager.getConnection(url,login,pwd);
        }catch(Exception e){
            
        }
    }

    public static Connection getConnection() {
        return connection;
    }
    
    public static DbConnection getInstance(){
        if(instance==null)
            instance=new DbConnection();
        return instance;
    }
    
}
