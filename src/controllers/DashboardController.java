/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.jfoenix.controls.JFXDrawer;
import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pidev_pre_version_0_1_1.Pidev_pre_version_011;



/**
 * FXML Controller class
 *
 * @author Sam
 */
public class DashboardController implements Initializable {

   @FXML
    private JFXDrawer drawer;

    @FXML
    private Hyperlink input_cnx;

    @FXML
    BorderPane borderPane;
    
    
    
    
    @FXML
    public void showDrawer(MouseEvent me) {
        
        if (!drawer.isShown()) {
            drawer.open();
        }
    }
    
        @FXML
        public void hideDrawer(MouseEvent me) {
        drawer.close();
        }

        @FXML
        public void getAcceuil(ActionEvent av){
       try {
           Parent root = FXMLLoader.load(getClass().getResource("/views/acceuil.fxml"));
           borderPane.setCenter(root);
       } catch (IOException ex) {
           Logger.getLogger(DashboardController.class.getName()).log(Level.SEVERE, null, ex);
       }
        }
        
        @FXML
        public void getRandonnee(ActionEvent av){
          
            
            //borderPane.getChildren().remove(middleAnchorPane);
            
            Platform.runLater(new Runnable() {
            @Override
            public void run() {
             try {
                
                   System.out.println("ajout randonnee is opening");
                    Parent root = FXMLLoader.load(getClass().getResource("/views/ajoutRandonnee.fxml"));
                    
              
                       borderPane.setCenter(root);
                       
           } catch (IOException ex) {
           Logger.getLogger(DashboardController.class.getName()).log(Level.SEVERE, null, ex);
            }
            }
          });          
       

        }
        
        @FXML
    void getListRando(ActionEvent event) {
       try {
           Parent root = FXMLLoader.load(getClass().getResource("/views/ListRandonnees.fxml"));
           borderPane.setCenter(root);
       } catch (IOException ex) {
           Logger.getLogger(DashboardController.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
        

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      try{
        Parent root = FXMLLoader.load(getClass().getResource("/views/connect.fxml"));
            Scene scene = new Scene(root);
            Pidev_pre_version_011.loginstage=new Stage();
            Pidev_pre_version_011.loginstage.setScene(scene);
            Pidev_pre_version_011.loginstage.setMaximized(true);
            Pidev_pre_version_011.loginstage.showAndWait();
        }catch(Exception e){
            
        }
           
           try {
               drawer.setSidePane(drawer.getChildren().get(1));
               
               Parent root = FXMLLoader.load(getClass().getResource("/views/acceuil.fxml"));
               borderPane.setCenter(root);
           } catch (IOException ex) {
               Logger.getLogger(DashboardController.class.getName()).log(Level.SEVERE, null, ex);
           }
           
           
           
           
           
  
        
        
        }
          //  ex.printStackTrace();
        ///}
  //  }

}
