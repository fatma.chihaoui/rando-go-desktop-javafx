/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.jfoenix.controls.JFXButton;
import entities.Randonnee;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import pidev_pre_version_0_1_1.Pidev_pre_version_011;
import services.RandonneeDAO;

/**
 * FXML Controller class
 *
 * @author yes-man
 */
public class AcceuilController implements Initializable {

    

    @FXML
    private ImageView image1;

    @FXML
    private Label titre1;

    @FXML
    private Label destination1;

    @FXML
    private Label prix1;

    @FXML
    private ImageView image2;

    @FXML
    private Label titre2;

    @FXML
    private Label destination2;

    @FXML
    private Label prix2;

    @FXML
    private ImageView image3;

    @FXML
    private Label titre3;

    @FXML
    private Label destination3;

    @FXML
    private Label prix3;
    
    @FXML
    private HBox list_HBox;
    
    List<Randonnee> listRando;
    
    
    @FXML
    void tokeToAffichageRandonnee(ActionEvent event) {
       try {
           
            int i=list_HBox.getChildren().indexOf(((JFXButton)event.getSource()).getParent());
            AffichageRandonneeController.init(listRando.get(i));
            BorderPane borderPane=(BorderPane)Pidev_pre_version_011.stage.getScene().getRoot();
            Parent root = FXMLLoader.load(getClass().getResource("/views/AffichageRandonnee.fxml"));
            borderPane.setCenter(root);
        } catch (IOException ex) {
            Logger.getLogger(AcceuilController.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    
    
        }
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
      
        // TODO
        
        
        List<Randonnee> l=new RandonneeDAO().affichertous(0);
        listRando=l.subList(l.size()-3,l.size());
        //listRando=l;
        Randonnee rando1=listRando.get(0);
        Randonnee rando2=listRando.get(1);
        Randonnee rando3=listRando.get(2);
        
        
        titre1.setText("titre:"+rando1.getTitre());
        titre2.setText("titre:"+rando2.getTitre());
        titre3.setText("titre:"+rando3.getTitre());
        
        
        destination1.setText("description:"+rando1.getTitre());
        destination2.setText("description:"+rando2.getTitre());
        destination3.setText("description:"+rando3.getTitre());
        
        prix1.setText("prix: "+rando1.prix);
        prix2.setText("prix: "+rando2.prix);
        prix3.setText("prix: "+rando3.prix);
        
        image1.setImage(new Image("http://localhost/PIDdevMobile/untitled/web/Uploads/"+rando1.getListImages().get(0).getPath()));
        image2.setImage(new Image("http://localhost/PIDdevMobile/untitled/web/Uploads/"+rando2.getListImages().get(0).getPath()));
        image3.setImage(new Image("http://localhost/PIDdevMobile/untitled/web/Uploads/"+rando3.getListImages().get(0).getPath()));
        
    }    
    
}
