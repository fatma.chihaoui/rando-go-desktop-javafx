/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import entities.Randonnee;
import entities.Tache;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import pidev_pre_version_0_1_1.Pidev_pre_version_011;
import static controllers.AffichageRandonneeController.rando;
import services.RandonneeDAO;
import utilWindows.PopUpWindow;

/**
 * FXML Controller class
 *
 * @author yes-man
 */
public class ModifierRandonneeController implements Initializable {
    static Randonnee rando;
    
    @FXML
    private JFXTextField titre_TextField;
    
    @FXML
    private JFXTextField prix_TextField1;
    
    @FXML
    private JFXTextField nbmin_TextField;

    @FXML
    private JFXTextField nbmax_TextField;

    @FXML
    private JFXTextField nbkilo_TextField;

    @FXML
    private JFXDatePicker date_Picker;

    @FXML
    private JFXTextField destination_TextField;

    @FXML
    private JFXTextArea desc_TextArea;

    @FXML
    private VBox listTache_VBox;
    
    String errMessage;
    
    @FXML
    void executeModefication(ActionEvent event) {
        if(executeVerification()==true){
            try {
                rando.setTitre(titre_TextField.getText());
                rando.prix=Integer.parseInt(prix_TextField1.getText());
                rando.setDescription(desc_TextArea.getText());
                rando.setNbKilometres(Float.parseFloat(nbkilo_TextField.getText()));
                rando.setNbPlacesMax(Integer.parseInt(nbmax_TextField.getText()));
                rando.setNbPlacesMin(Integer.parseInt(nbmin_TextField.getText()));
                rando.setDate_rando(Date.valueOf(date_Picker.getValue()));
                System.out.println("valid");
                new RandonneeDAO().modifierRandonnee(rando);
                new PopUpWindow("info","modefication effectuee");
                
                BorderPane borderPane=(BorderPane)Pidev_pre_version_011.stage.getScene().getRoot();
                Parent root = FXMLLoader.load(getClass().getResource("/views/acceuil.fxml"));
            } catch (IOException ex) {
                Logger.getLogger(ModifierRandonneeController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        else{
            System.out.println("invalid");
            new PopUpWindow("erreur","une erreur dans le champ "+errMessage);
        }
        
    }
    
    private boolean executeVerification(){
    
        boolean test=true;
        if(titre_TextField.getText().length()<=0 ){
            test=false;
            errMessage="titre";return test;
        }
        if(desc_TextArea.getText().length()<=0){
            test=false;errMessage="description";return test;
        }
        if(nbkilo_TextField.getText().length()<=0 || !nbkilo_TextField.getText().matches("[0-9]+[.]?[0-9]+")){
            test=false;errMessage="nombre de kilometres";return test;
            
        }
        if(nbmax_TextField.getText().length()<=0 || !nbmax_TextField.getText().matches("[0-9]+")){
            test=false;errMessage="nombre de participants max";return test;
        }
        if(nbmin_TextField.getText().length()<=0  || !nbmin_TextField.getText().matches("[0-9]+")){
            test=false;errMessage="nombre de participants min";return test;
        }
        if(Integer.parseInt(nbmin_TextField.getText())>Integer.parseInt(nbmax_TextField.getText())){
            test=false;errMessage="nombre de participants min doit etre inferieur au nombre de participants max";return test;
        }
        if(date_Picker.getValue()==null){
            test=false;errMessage="date ";return test;
        }
        
       
        
        
        return test;
        
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        prix_TextField1.setText(rando.prix+"");
        titre_TextField.setText(rando.getTitre());
        desc_TextArea.setText(rando.getDescription());
        nbkilo_TextField.setText(rando.getNbKilometres()+"");
        nbmax_TextField.setText(rando.getNbPlacesMax()+"");
        nbmin_TextField.setText(rando.getNbPlacesMin()+"");
        date_Picker.setValue(rando.getDate_rando().toLocalDate());
        destination_TextField.setText(rando.getDestination().getNom());
        
        for(Tache ta :rando.getListTaches()){
            Label tacheNumber=new Label((rando.getListTaches().indexOf(ta)+1)+"");
            tacheNumber.setPrefSize(25, 0);
            
            Label startDate=new Label(ta.getHeureDebut().get(Calendar.DAY_OF_MONTH)+"/"+ta.getHeureDebut().get(Calendar.MONTH)+"/"+ta.getHeureDebut().get(Calendar.YEAR)+" "+ta.getHeureDebut().get(Calendar.HOUR_OF_DAY)+":"+ta.getHeureDebut().get(Calendar.MINUTE));
            startDate.setPrefSize(125, 0);
            
            Label description=new Label(ta.getDescription());
            description.setPrefSize(350, 0);
            
            HBox hbox=new HBox(tacheNumber,startDate,description);
            hbox.setPrefSize(500, 0);
            
            listTache_VBox.getChildren().add(hbox);
            
            
            
        }
        
        
    }    
    
    
    public static void init(Randonnee randonnee){
        rando=randonnee;
    }
    
}
