/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapTypeIdEnum;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;
import entities.Destination;
import entities.Commentaire;
import entities.ConnectedUser;
import entities.Randonnee;
import entities.RandonneeImage;
import entities.Tache;
import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import pidev_pre_version_0_1_1.Pidev_pre_version_011;
import services.CommentaireDAO;
import services.RandonneeDAO;
import utilWindows.PopUpWindow;

/**
 * FXML Controller class
 *
 * @author yes-man
 */
public class AffichageRandonneeController implements Initializable {
    
    static Randonnee rando;
    
    @FXML
    private GoogleMapView googleMapView;
    private GoogleMap map;

    
    @FXML
    JFXButton modifier_Button;
    
    @FXML
    private Label Titre_label;

    @FXML
    private ImageView main_imageView;

    @FXML
    private FlowPane image_flowPane;

    @FXML
    private Label typeRando_label;

    @FXML
    private Label nbpartici_label;

    @FXML
    private Label dateRando_label;

    @FXML
    private Label destination_label;

    @FXML
    private Label nbKilo_label;

    @FXML
    private Label organisateur_label;

    @FXML
    private Text Description_text;
    
    @FXML
    private JFXTextField cmt_textfield;

    //@FXML
    //private GoogleMapView googleMapView;

    @FXML
    private VBox listTache_VBox;
    
    @FXML
    private VBox mainCommentVBox;
    
    @FXML
    void supprimerRandonnee(ActionEvent event) {
        try {
            new RandonneeDAO().supprimerRandonnee(rando);
            new PopUpWindow("suppression","randonnee supprimer");
            BorderPane borderPane=(BorderPane)Pidev_pre_version_011.stage.getScene().getRoot();
            Parent root = FXMLLoader.load(getClass().getResource("/views/acceuil.fxml"));
            borderPane.setCenter(root);
        } catch (IOException ex) {
            Logger.getLogger(AffichageRandonneeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    void modifierRandonnee(ActionEvent event) {
        
        try {
            ModifierRandonneeController.init(rando);
            BorderPane borderPane=(BorderPane)Pidev_pre_version_011.stage.getScene().getRoot();
            Parent root = FXMLLoader.load(getClass().getResource("/views/modifierRandonnee.fxml"));
            borderPane.setCenter(root);
        } catch (IOException ex) {
            Logger.getLogger(AffichageRandonneeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    @FXML
    void sendComment(ActionEvent event){
        Commentaire c=new Commentaire();
        c.setContenu(cmt_textfield.getText());
        
        c.setId_rando(rando.getId());
        c.setLogin_user(ConnectedUser.getId());
        c.setUsername(ConnectedUser.getUsername());
        (new CommentaireDAO()).AjouterCommentaire(c);
        
        Label username=new Label(c.getUsername()+":");
            username.setFont(new Font(21));
            
            Label msg=new Label(c.getContenu());
            
            VBox vb=new VBox(username,msg);
            mainCommentVBox.getChildren().add(mainCommentVBox.getChildren().size()-1,vb);
        
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println(rando);
        Titre_label.setText(rando.getTitre());
        Description_text.setText(rando.getDescription());
        destination_label.setText(rando.getDestination().getNom());
        dateRando_label.setText(rando.getDate_rando().toString());
        nbpartici_label.setText(rando.getNbPlacesMax()+"");
        typeRando_label.setText(rando.getTypeRandonnee().getType());
        nbKilo_label.setText(rando.getNbKilometres()+"");
        organisateur_label.setText(rando.getLoginOrganisateur().getUsername());
        
        
        main_imageView.setImage(new Image("http://localhost/PIDdevMobile/untitled/web/Uploads/"+rando.getListImages().get(0).getPath()));
        main_imageView.setOnMouseClicked(eh->{
            Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
            Stage fullScreenImage=new Stage();
            fullScreenImage.setMaximized(true);
            ImageView iv=new ImageView();
            iv.setFitHeight(primaryScreenBounds.getHeight());
            iv.setFitWidth(primaryScreenBounds.getWidth());
            iv.setImage(main_imageView.getImage());
            fullScreenImage.setScene(new Scene(new AnchorPane(iv)));
            fullScreenImage.showAndWait();
        });
        
        
        for(RandonneeImage ri : rando.getListImages()){
            ImageView iv=new ImageView(new Image("http://localhost/PIDdevMobile/untitled/web/Uploads/"+ri.getPath()));
            iv.setFitHeight(100);
            iv.setFitWidth(100);
            iv.setOnMouseClicked(eh->{
            main_imageView.setImage(((ImageView)eh.getSource()).getImage());
            });            
            image_flowPane.getChildren().add(iv);
        }
        
        
        for(Tache ta :rando.getListTaches()){
            Label tacheNumber=new Label((rando.getListTaches().indexOf(ta)+1)+"");
            tacheNumber.setPrefSize(25, 0);
            
            Label startDate=new Label(ta.getHeureDebut().get(Calendar.DAY_OF_MONTH)+"/"+ta.getHeureDebut().get(Calendar.MONTH)+"/"+ta.getHeureDebut().get(Calendar.YEAR)+" "+ta.getHeureDebut().get(Calendar.HOUR_OF_DAY)+":"+ta.getHeureDebut().get(Calendar.MINUTE));
            startDate.setPrefSize(125, 0);
            
            Label description=new Label(ta.getDescription());
            description.setPrefSize(350, 0);
            
            HBox hbox=new HBox(tacheNumber,startDate,description);
            hbox.setPrefSize(500, 0);
            
            listTache_VBox.getChildren().add(hbox); 
        }
        
        for(Commentaire co : (new CommentaireDAO()).lister(rando.getId())){
            Label username=new Label(co.getUsername()+":");
            username.setFont(new Font(21));
            
            Label msg=new Label(co.getContenu());
            
            VBox vb=new VBox(username,msg);
            mainCommentVBox.getChildren().add(mainCommentVBox.getChildren().size()-1,vb);
        }
        
        
        
        
        initmaps(rando.getDestination());
        
        
    }    
    
    public void initmaps(Destination destination){
        googleMapView.addMapInializedListener(() -> {
        
            LatLong center = new LatLong(destination.getPointDeLocalisation().getLattitude(),destination.getPointDeLocalisation().getLongitude());
        System.out.println("creating center "+center);
        MapOptions options = new MapOptions();
        options.center(center)
                .mapMarker(true)
                .zoom(8)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .scaleControl(false)
                .streetViewControl(false)
                .zoomControl(true)
                .mapType(MapTypeIdEnum.ROADMAP);
        System.out.println("creating options "+options);

        map = googleMapView.createMap(options,false);
        System.out.println("map created"+map);
        
        
        MarkerOptions mo=new MarkerOptions();
             mo.position(center);
             Marker marker=new Marker(mo);
             map.addMarker(marker);
        
        
        });
    }
    
    public static void init(Randonnee Rando){
        rando=Rando;
    }
    
   
    
    
}
