/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapTypeIdEnum;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;
import entities.ConnectedUser;
import entities.Destination;
import entities.Localisation;
import entities.Randonnee;
import entities.RandonneeImage;
import entities.Tache;
import entities.TypeRandonnee;
import entities.Utilisateur;
import netscape.javascript.JSObject;

import java.io.File;

import java.io.IOException;

import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import pidev_pre_version_0_1_1.Pidev_pre_version_011;
import services.TypeRandonneeDAO;
import utilWindows.KeyConfirmationWindow;
import utilWindows.PopUpWindow;





/**
 * FXML Controller class
 *
 * @author yes-man
 */
public class AjoutRandonneeController implements Initializable {
    
    private String errMessage="";
    
    @FXML
    JFXDatePicker heurDebut_datePicker;
    
    @FXML
    private JFXButton reloadButton;

    @FXML
    private HBox photo_scroll_pane;


    @FXML
    private TextField descTache_textField;
    
    @FXML
    private TextField prix_textField;

    @FXML
    private JFXButton fileChooser;
    
    @FXML
    private JFXTextField nbKilom_textField;

    @FXML
    private JFXDatePicker dateRandonnee_picker;

    @FXML
    private JFXSlider nombreMax_slider;

    @FXML
    private JFXSlider nombreMin_slider;
   

    @FXML
    private JFXTextField destination_textField;

    @FXML
    private JFXTextArea description_textField;

    @FXML
    private JFXTextField titre_textField;

    @FXML
    private VBox listTaches_VBox;
    
    @FXML
    private GoogleMapView googleMapView;
    
    @FXML
    ChoiceBox<TypeRandonnee> randonneeType_choiceBox;
            
    List<RandonneeImage> listImages;
    List<Tache> listTaches;
    Destination destination;
    TypeRandonnee selectedTypeRandonnee;
    Marker marker;
    
    private GoogleMap map;
    
    
    @FXML
    public void ajoutRandonnee(ActionEvent e){
        if(fieldValidation()==false){
            
            new PopUpWindow("erreur",errMessage);
        }else{
            //init randonnee
        Randonnee randonnee=new Randonnee();
        //init with static values
        Utilisateur u=new Utilisateur();
        u.id=ConnectedUser.getId();
        u.setUsername(ConnectedUser.getUsername());
        u.setEmail("atef.rebai@esprit.tn");
        randonnee.setLoginOrganisateur(u);
        randonnee.setEtat(0);
        
        randonnee.setTitre(titre_textField.getText());
        randonnee.prix=Integer.parseInt(prix_textField.getText());
        randonnee.setDescription(description_textField.getText());
        randonnee.setNbPlacesMin((int) nombreMin_slider.getValue());
        randonnee.setNbPlacesMax((int) nombreMax_slider.getValue());
        randonnee.setDate_rando(Date.valueOf(dateRandonnee_picker.getValue()));
        randonnee.setListImages(listImages);
        randonnee.setListTaches(listTaches);
        randonnee.setDestination(destination);
        randonnee.setTypeRandonnee(selectedTypeRandonnee);
        randonnee.setNbKilometres(Float.parseFloat(nbKilom_textField.getText()));
       
        
            try {
            //ajouter et faire l'upload des images au serv
            KeyConfirmationWindow kcw=new KeyConfirmationWindow(randonnee);
            kcw.show();
            ((BorderPane)Pidev_pre_version_011.stage.getScene().getRoot()).setCenter(FXMLLoader.load(getClass().getResource("/views/acceuil.fxml")));
            } catch (IOException ex) {
                Logger.getLogger(AjoutRandonneeController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        
        
        
        }    
    }
    
    @FXML
    public void setChosenFiles(ActionEvent e) throws IOException, InterruptedException{
        FileChooser f=new FileChooser();
        f.getExtensionFilters().add(
        new ExtensionFilter("Image Files","*.jpg","*.png")
        );
         List<File> files;
        files=f.showOpenMultipleDialog(new Stage());
        
        if(files!=null){
            for(File file : files){ 
                listImages.add(new RandonneeImage(file.getAbsolutePath())); 
                //init photo
                Image i= new Image(file.toURI().toURL().toString());ImageView imageview1=new ImageView(i);imageview1.setFitHeight(250);imageview1.setFitWidth(250);
                //adding photo
                photo_scroll_pane.getChildren().add(imageview1);
                //set delete listener
                imageview1.setOnMouseClicked(b->{
                    //remove image from list
                    listImages.remove(photo_scroll_pane.getChildren().indexOf(b.getSource()));
                    //remove imageview from pane
                    photo_scroll_pane.getChildren().remove(b.getSource());
                });
            }
        }   
    }
   
    @FXML
    public void ajoutTache(ActionEvent e){
        if(descTache_textField.getText().length()<=0){
            new PopUpWindow("erreur", "la tache ne doit pas etre vide");
            return ;
        }
        try{
        Tache tache=new Tache();
        Label nameLabel=new Label("tache "+(((listTaches_VBox.getChildren().size()-1)/2)+1));
        nameLabel.setMinWidth(40);
        Label hdeb=new Label(heurDebut_datePicker.getTime().getHour()+":"+heurDebut_datePicker.getTime().getMinute());
        hdeb.setMinWidth(72);
        Calendar cal=Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY,heurDebut_datePicker.getTime().getHour());
        cal.set(Calendar.MINUTE,heurDebut_datePicker.getTime().getMinute());
        tache.setHeureDebut(cal);
        tache.setHeureFin(cal);
        Label desc=new Label(descTache_textField.getText());
        desc.setMinWidth(377);
        tache.setDescription(descTache_textField.getText());

        Button deleteButton=new Button("X");

        //init hbox
        HBox hbox=new HBox(5,nameLabel,hdeb,desc,deleteButton);
        listTaches_VBox.getChildren().add(listTaches_VBox.getChildren().size()-1,hbox);
        listTaches_VBox.getChildren().add(listTaches_VBox.getChildren().size()-1,new Separator());
        // if hbox clicked
        hbox.setOnMouseClicked(a->{
            //color entry
            for(Node n : listTaches_VBox.getChildren()){
                if(n instanceof HBox){
                    ((HBox)n).setStyle("-fx-background-color:inherit");
                }
            }  
        ((HBox)a.getSource()).setStyle("-fx-background-color:red");
        });
        
        //set delete button
        deleteButton.setOnAction(a->{
            //delete entry from list
            listTaches.remove(listTaches_VBox.getChildren().indexOf(hbox)/2); 
            //delete separator from vbox
            listTaches_VBox.getChildren().remove(listTaches_VBox.getChildren().indexOf(hbox)+1);
            //delete item from vbox
            listTaches_VBox.getChildren().remove(hbox);
        
            //change label
            int i=1;
            for(Node n : listTaches_VBox.getChildren()){
                if(n instanceof HBox && i!=1 && i!= listTaches_VBox.getChildren().size()-1){
                    ((Label) ((HBox) n).getChildren().get(0)).setText("tache "+i);
                    i++;
                }
            }
        });
        //if all ok add tache to list
        listTaches.add(tache);
        }catch(Exception ex){
            new PopUpWindow("erreur","une erreur s'est produit dans la liste des tache");
            ex.printStackTrace();
        }
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        
        
    //affichage de la list de TypeRandonnee
    ObservableList<TypeRandonnee > choices = FXCollections.observableArrayList();
    for(TypeRandonnee tr : new TypeRandonneeDAO().affichertous()) {
      choices.add(tr);
    }
    randonneeType_choiceBox.setItems(choices);
    randonneeType_choiceBox.getSelectionModel().select(0);
    selectedTypeRandonnee=choices.get(0);
    randonneeType_choiceBox.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends TypeRandonnee > observableValue, TypeRandonnee  oldChoice, TypeRandonnee newChoice) -> {
        selectedTypeRandonnee=newChoice;
    });
    

    try{
    //init googleMaps
    googleMapView.addMapInializedListener(() -> {
       //Set the initial properties of the map.
        LatLong center = new LatLong(35.74651225991851,9.66796875);
        System.out.println("creating center "+center);
        MapOptions options = new MapOptions();
        options.center(center)
                .zoom(6)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .scaleControl(false)
                .streetViewControl(false)
                .zoomControl(true)
                .mapType(MapTypeIdEnum.ROADMAP);
        System.out.println("creating options "+options);

        map = googleMapView.createMap(options,false);
        System.out.println("map created"+map);

         map.addUIEventHandler(UIEventType.click, (netscape.javascript.JSObject obj) -> {
            LatLong ll = new LatLong((netscape.javascript.JSObject) obj.getMember("latLng")); 
            
             MarkerOptions mo=new MarkerOptions();
             mo.position(ll);
             
            map.clearMarkers();
            int currentZoom = map.getZoom();
            map.setZoom( currentZoom - 1 );
            map.setZoom( currentZoom );
            
            marker=new Marker(mo);
            map.addMarker(marker);

            System.out.println("marker added at "+ll.getLatitude()+","+ll.getLongitude());
            destination=new Destination(0,destination_textField.getText(),new Localisation(0,ll.getLatitude(),ll.getLongitude()));
        });
    });
    }catch(Exception ex){
        ex.printStackTrace();
        new PopUpWindow("erreur","google maps ne peut pas etre charge");
    }
    }   
    

    public AjoutRandonneeController(){
    listImages=new ArrayList<>();
    listTaches=new ArrayList<>();
    }
    
    public boolean fieldValidation(){
     if(titre_textField.getText().length()<=0){
        errMessage="titre est vide";return false;
     }
     if(destination_textField.getText().length()<=0){
        errMessage="destination est vide";return false;
     }
      if(marker==null){
        errMessage="vous devez choisir une destination sur la mape";return false;
     }
       if(description_textField.getText().length()<=0){
        errMessage="description est vide";return false;
     }
        if(nbKilom_textField.getText().length()<=0 || !nbKilom_textField.getText().matches("[0-9]+[.]?[0-9]+")){
        errMessage="nombre de kilometres est invalide";return false;
     }
         if(nombreMin_slider.getValue()>nombreMax_slider.getValue()){
        errMessage="nombre de participants est invalide";return false;
     }
         if(dateRandonnee_picker.getValue()==null){
        errMessage="vous devez choisir une date";return false;
     }
         if(listImages.isEmpty()){
        errMessage="vous devez choisir une image";return false;
     }
         if(listTaches.isEmpty()){
        errMessage="vous devez choisir une tache";return false;
     }
       
         
     
     
        
     return true;
    }
    
 
    
      
    
    
    
    
}
