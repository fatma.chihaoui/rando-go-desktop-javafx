/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.jfoenix.controls.JFXButton;
import entities.Randonnee;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import services.RandonneeDAO;

/**
 * FXML Controller class
 *
 * @author yes-man
 */
public class ListRandonneesController implements Initializable {

    @FXML
    VBox listRando_VBox;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        List<Randonnee> listRandonnee=new RandonneeDAO().affichertous(0);
        System.out.println(listRandonnee.size());
        for(Randonnee rando : listRandonnee){
            
            HBox hbox1=new HBox();
            hbox1.setAlignment(Pos.CENTER_LEFT);
            hbox1.setPadding(new Insets(20, 20, 20, 20));
            hbox1.setSpacing(20);
            
            //hbox1.setMaxSize(900, 150);
            
            ImageView imageView =new ImageView();
            imageView.setFitHeight(100);imageView.setFitWidth(100);
            imageView.setImage(new Image("http://localhost/PIDdevMobile/untitled/web/Uploads/"+rando.getListImages().get(0).getPath()));
            
            
            VBox vbox2=new VBox();
            vbox2.setSpacing(20);
            
            Text titre=new Text(rando.getTitre());titre.setWrappingWidth(450);
            Text desc=new Text(rando.getDescription());desc.setWrappingWidth(450);
            
            vbox2.getChildren().add(titre);
            vbox2.getChildren().add(desc);
            
            hbox1.getChildren().add(imageView);
            hbox1.getChildren().add(vbox2);
            
            JFXButton button=new JFXButton("lire plus");
            button.setStyle("-fx-background-color:#28a6ea");
            
            hbox1.getChildren().add(button);
            
            listRando_VBox.getChildren().add(hbox1);
            
            
        }
        
        
    }    
    
}
