/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import services.UtilisateurDAO;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import entities.ConnectedUser;
import entities.Utilisateur;
import java.io.IOException;
import java.net.URL;

import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.scene.control.Alert;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import pidev_pre_version_0_1_1.Pidev_pre_version_011;




/**
 *
 * @author rihab_pc
 */
public class ConnexionController implements Initializable {
 
    @FXML
    private ImageView idUtilisateur;
    
    
    @FXML
    private ImageView idGuide;
    
    @FXML
    private JFXTextField loginUser;
    @FXML
    private JFXPasswordField pwdUser;

    
 
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    
        @FXML
    void login(ActionEvent event) throws IOException {
    (new UtilisateurDAO()).connectUser(loginUser.getText(),pwdUser.getText());
    
    System.out.println(ConnectedUser.getUsername());
    System.out.println(ConnectedUser.getId());
    
if(ConnectedUser.getId() == 0){
    System.out.println("not connected");
}else{
      Pidev_pre_version_011.loginstage.close();
      
}
    
}

}