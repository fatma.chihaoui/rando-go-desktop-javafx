/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pidev_pre_version_0_1_1;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import utils.DbConnection;


/**
 *
 * @author yes-man
 */



public class Pidev_pre_version_011 extends Application {

public static Stage stage;

public static Stage loginstage;


    
    @Override
    public void start(Stage stage) throws Exception {
        
        this.stage=stage;
        
        if(DbConnection.getInstance().getConnection()==null){
            stage.setMinHeight(150);stage.setMinWidth(150);
            VBox vbox=new VBox(new Label("no database connection"));
            vbox.setAlignment(Pos.CENTER);
            vbox.setPrefSize(250, 250);
           stage.setScene(new Scene(vbox));
           stage.show();
            
        }else{
            
            //Parent root=FXMLLoader.load(getClass().getResource("/views/connect.fxml"));
            
            Parent root = FXMLLoader.load(getClass().getResource("/views/Dashboard.fxml"));
            Scene scene = new Scene(root);
            stage.setScene(scene);
           //stage.setMaximized(true);
            stage.show();
        }

//        


        



//  test DAO LocalisationDAO valide
//        LocalisationDAO.ajouter(new Localisation(0,"tunis",10.10d,20.20d));
//        LocalisationDAO.modifier(new Localisation(1,"tunis",100.10d,200.20d));
//        Localisation l=LocalisationDAO.afficher(1);
//        System.out.println(l.getId()+" "+l.getLibelle()+" "+l.getLattitude()+" "+l.getLongitude());
//          LocalisationDAO.supprimer(new Localisation(1,"tunis",10.10d,20.20d));
//          LocalisationDAO.supprimer(new Localisation(2,"tunis",10.10d,20.20d));
//          LocalisationDAO.supprimer(new Localisation(3,"tunis",10.10d,20.20d));

// test DAO TypeRandonneeDAO  valide
//        TypeRandonneeDAO.ajouter(new TypeRandonnee(1,"swimming"));
//        TypeRandonneeDAO.ajouter(new TypeRandonnee(2,"hiking"));
//        TypeRandonneeDAO.ajouter(new TypeRandonnee(3,"no93ed f dar w netfarej 5ir"));
//        TypeRandonneeDAO.modifier(new TypeRandonnee(2,"hiking for a better life"));
//        TypeRandonnee tr=TypeRandonneeDAO.afficher(3);
//        System.out.println(tr.getId()+" "+tr.getType());
//        for(TypeRandonnee t : TypeRandonneeDAO.affichertous()){
//            System.out.println(t.getId()+" "+t.getType());
//        }
//          TypeRandonneeDAO.supprimer(new TypeRandonnee(1,"swimming"));
//          TypeRandonneeDAO.supprimer(new TypeRandonnee(2,"swimming"));
//          TypeRandonneeDAO.supprimer(new TypeRandonnee(3,"swimming"));



// test DAO RandonneeImageDAO  valide
// to delete RandonneeImageDAO.afficher(id) non utilisable
// to add RandonneeImageDAO.supprimer(Randonnee) plus utile
//        Randonnee r=new Randonnee();
//        r.setId(1);
//        RandonneeImageDAO.ajouter(new RandonneeImage(1,"c:/Temp/1.jpg"),r);
//        RandonneeImageDAO.ajouter(new RandonneeImage(2,"c:/Temp/2.jpg"),r);
//        RandonneeImageDAO.ajouter(new RandonneeImage(3,"c:/Temp/3.jpg"),r);
//        RandonneeImageDAO.modifier(new RandonneeImage(15,"c:/Temp/22.jpg"));
//        RandonneeImage tr=RandonneeImageDAO.afficher(15);
//        System.out.println(tr.getId()+" "+tr.getPath());
//        for(RandonneeImage t : RandonneeImageDAO.affichertous(r)){
//            System.out.println(t.getId()+" "+t.getPath());
//        }
//          RandonneeImageDAO.supprimer(new RandonneeImage(14,"swimming"));
//          RandonneeImageDAO.supprimer(new RandonneeImage(15,"swimming"));
//          RandonneeImageDAO.supprimer(new RandonneeImage(16,"swimming"));

/*
Destination d =new Destination(520,"sfax",new Localisation(750,"c'est la ville de sfax",572.2d,5875.21402d));
DestinationDAO.ajouter(d);*/

//
//Localisation l=new Localisation(0,"c'est la localisation de la ville de tunis", 120, 520);
//Destination d=new Destination(0,"c'est une destination:tunis", l);
//TypeRandonnee tr=new TypeRandonnee(1,"c'est un type de randonnee");
//RandonneeImage ri1=new RandonneeImage(0, "path to image1");
//RandonneeImage ri2=new RandonneeImage(0, "path to image2");
//RandonneeImage ri3=new RandonneeImage(0, "path to image3");
//List<RandonneeImage>list =new ArrayList<RandonneeImage>();
//list.add(ri1);list.add(ri2);list.add(ri3);
//Utilisateur u=new Utilisateur();
//Randonnee r=new Randonnee(0,"titre du randonnee","description du randonnee",20, 100,new Date(2002, 10, 10),20.5f,"en att", list,null, d, tr, u);



//RandonneeDAO.ajouterRandonnee(r);
//for(Randonnee r : RandonneeDAO.affichertous("en att")){
//    System.out.println(r);
//}


//test TacheDAO
//Localisation l1=new Localisation(0,"label1", 10, 10);
//Localisation l2=new Localisation(0,"label2", 100, 100);
//Localisation l3=new Localisation(0,"label3", 110, 110);
//
//Tache t1=new Tache(0,"description",Calendar.getInstance(),Calendar.getInstance(),null);
//Tache t2=new Tache(0,"description",Calendar.getInstance(),Calendar.getInstance(),null);
//Tache t3=new Tache(0,"description",Calendar.getInstance(),Calendar.getInstance(),null);
  //      TacheDAO.ajout(t1,35);
    //    TacheDAO.ajout(t2,35);
      //  TacheDAO.ajout(t3,35);
//        List<Tache> list=TacheDAO.afficherTousParIdRando(35);
//        for(Tache t : list){
//            System.out.println(t);
//        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
